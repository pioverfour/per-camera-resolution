# Copyright (C) 2019-2023 Damien Picard dam.pic AT free.fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name": "Per-camera resolution",
    "author": "Damien Picard",
    "version": (1, 1),
    "blender": (2, 81, 0),
    "location": "Properties > Object Data (Camera) > Camera",
    "description": "Allows per-camera resolution",
    "warning": "Does not work when rendering animation",
    "wiki_url": "",
    "category": "Camera",
}


import bpy
from bpy.app.handlers import persistent

def update_render(cam_data, render):
    """Update scene resolution"""
    render.resolution_x = cam_data.resolution_x
    render.resolution_y = cam_data.resolution_y
    render.resolution_percentage = cam_data.resolution_percentage

    render.pixel_aspect_x = cam_data.pixel_aspect_x
    render.pixel_aspect_y = cam_data.pixel_aspect_y


def update_resolution_prop(self, context):
    """Update scene resolution when the camera resolution is updated"""
    if context.object is context.scene.camera:
        render = context.scene.render
        cam_data = context.scene.camera.data

        update_render(cam_data, render)


@persistent
def update_resolution_handler(scene):
    """Update scene resolution on frame change (eg. playback)"""
    if scene.camera is not None:
        render = scene.render
        cam_data = scene.camera.data

        update_render(cam_data, render)


def draw_cam_resolution(self, context):
    """Draw camera resolution in the Camera panel """
    cam = context.camera
    layout = self.layout

    col = layout.column(align=True)
    col.prop(cam, "resolution_x", text="Resolution X")
    col.prop(cam, "resolution_y", text="Y")
    col.prop(cam, "resolution_percentage", text="%")

    col = layout.column(align=True)
    col.prop(cam, "pixel_aspect_x", text="Aspect X")
    col.prop(cam, "pixel_aspect_y", text="Y")


def register():
    bpy.types.Camera.resolution_x = bpy.props.IntProperty(
        name="Resolution X", description="Number of pixels in the render width for this camera",
        default=1920, min=1,
        options={'ANIMATABLE', 'PROPORTIONAL'},
        subtype="PIXEL",
        update=update_resolution_prop)
    bpy.types.Camera.resolution_y = bpy.props.IntProperty(
        name="Resolution Y", description="Number of pixels in the render height for this camera",
        default=1080, min=1,
        options={'ANIMATABLE', 'PROPORTIONAL'},
        subtype="PIXEL",
        update=update_resolution_prop)
    bpy.types.Camera.resolution_percentage = bpy.props.IntProperty(
        name="Resolution Y", description="Multiplier for the camera's resolution",
        default=100, min=1, soft_max=100,
        subtype="PERCENTAGE",
        update=update_resolution_prop)
    bpy.types.Camera.pixel_aspect_x = bpy.props.FloatProperty(
        name="Pixel Aspect X", description="Horizontal aspect ratio - for anamorphic or non-square pixel output",
        default=1.0, min=1.0, max=200.0,
        update=update_resolution_prop)
    bpy.types.Camera.pixel_aspect_y = bpy.props.FloatProperty(
        name="Pixel Aspect Y", description="Vertical aspect ratio - for anamorphic or non-square pixel output",
        default=1.0, min=1.0, max=200.0,
        update=update_resolution_prop)


    bpy.types.DATA_PT_camera.append(draw_cam_resolution)
    bpy.app.handlers.depsgraph_update_post.append(update_resolution_handler)
    bpy.app.handlers.frame_change_post.append(update_resolution_handler)


def unregister():
    del bpy.types.Camera.resolution_x
    del bpy.types.Camera.resolution_y
    del bpy.types.Camera.resolution_percentage

    bpy.types.DATA_PT_camera.remove(draw_cam_resolution)
    bpy.app.handlers.depsgraph_update_post.remove(update_resolution_handler)
    bpy.app.handlers.frame_change_post.remove(update_resolution_handler)


if __name__ == "__main__":
    register()
